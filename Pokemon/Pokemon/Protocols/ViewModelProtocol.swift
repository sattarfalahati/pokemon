//
//  ViewModelProtocol.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import Foundation

public protocol ViewModelProtocol {
    associatedtype Model
    
    var model: Model { get }
    
    init(_ model: Model)
}
