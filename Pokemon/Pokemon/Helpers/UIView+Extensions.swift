//
//  UIView+Extensions.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import UIKit

extension UIView {
    
    /// Transform the layer to a circle.
    func layerAsCircle() {
        self.layer.cornerRadius = self.frame.height/2
    }
    
    /// Make constraints expand to superview.
    func constraintToSuperview(insets: UIEdgeInsets = .zero) {
        guard let superview = self.superview else {
            assert(false, "Error! `superview` was nil – call `addSubview(_ view: UIView)` before calling `\(#function)` to fix this.")
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: superview.topAnchor, constant: insets.top),
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: insets.bottom),
            leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: insets.left),
            trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: insets.right)
        ])
    }
    
    /// Center the view into its superview keeping the original size.
    func centerInSuperview() {
        guard let superview = self.superview else {
            assert(false, "Error! `superview` was nil – call `addSubview(_ view: UIView)` before calling `\(#function)` to fix this.")
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            centerXAnchor.constraint(equalTo: superview.centerXAnchor),
            centerYAnchor.constraint(equalTo: superview.centerYAnchor)
        ])
    }
    
    /// Remove all subviews from a view.
    @discardableResult
    func removeAllSubviews() -> [UIView] {
        let subviewsCopy = self.subviews
        subviewsCopy.forEach {
            $0.removeFromSuperview()
        }
        return subviewsCopy
    }
    
    func removeConstraintsIfNotNil(_ constraints: [NSLayoutConstraint?]) {
        self.removeConstraints(constraints.compactMap( { $0 }))
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    /// Add self into parent and fill up to its size.
    ///
    /// - Parameters:
    ///   - parentView: parent container view.
    ///   - autoLayout: use autolayout constraints.
    ///   - safeArea: link to safe area, not superview.
    ///   - insets: optional insets from margins.
    func addIntoParent(_ parentView: UIView, autoLayout: Bool = true, safeArea: Bool = false, insets: UIEdgeInsets = .zero) {
        guard autoLayout else {
            parentView.addSubview(self)
            self.frame = parentView.bounds
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(self)
        
        if safeArea == false {
            self.constraintToSuperview(insets: insets)
        } else {
            let guide = parentView.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                self.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: insets.right),
                self.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: insets.left),
                self.topAnchor.constraint(equalTo: guide.topAnchor, constant: insets.top),
                self.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: insets.bottom)
            ])
        }
    }
    
}
