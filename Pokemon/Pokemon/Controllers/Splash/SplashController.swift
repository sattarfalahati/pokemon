//
//  SplashController.swift
//  Pokemon
//
//  Created by sattar.falahati on 29/11/2020.
//

import UIKit

public class SplashController: UIViewController {
    
    // MARK: Properties & Outlets
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "Pokemon")
        return imageView
    }()
    
    private let style = SplashController.Style()
    private let delay: TimeInterval = 2
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        addPokemonLogo()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.goToPokemonList()
        }
    }
    
    private func addPokemonLogo() {
        
        view.addSubview(imageView)
        
        // Add The Image Logo at the center
        imageView.centerInSuperview()
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: style.logoHeight)
        ])
    }
    
    private func goToPokemonList() {
        
        let listVC = ListController()
        let navigationController = UINavigationController(rootViewController: listVC)
        navigationController.modalPresentationStyle = .fullScreen
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true)
    }
}

