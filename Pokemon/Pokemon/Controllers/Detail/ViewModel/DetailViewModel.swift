//
//  DetailViewModel.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import Foundation
import PokemonAPI
import Hydra

public extension DetailController {
    
    class ViewModel: ViewModelProtocol {
        
        // Intial Model with the name of the pokemon
        public typealias Model = String
        public var model: String
        
        /// Data set.
        public private(set) var pokemon: PKMPokemon?
        
        public required init(_ model: String) {
            self.model = model
        }
        
        // MARK: - public functions
        
        // Get the pokemon detail
        public func getPokemonDetails() -> Promise<Void> {
            return fetchPokemon()
        }
        
        public func image() -> String? {
            return pokemon?.sprites?.backDefault
        }
        
        public func pokemonInfo() -> [String] {
            
            return [abilityName(),
                     typeName(),
                     weight(),
                     height()]
        }
        
        // MARK: - Private functions
        
        // Fetch
        private func fetchPokemon() -> Promise<Void> {
            
            return Promise<Void>(in: .background) { (resolve, reject, _) in
                PokemonAPI().pokemonService.fetchPokemon(self.model) { [weak self] result in
                    guard let self = self else { return }

                    switch result {
                    case .success(let pokemon):
                        self.pokemon = pokemon
                        resolve(Void())
                    case .failure(let error):
                        print(error.localizedDescription)
                        reject(error)
                    }
                }
            }
        }
        
        private func abilityName() -> String {
            guard let abilityName = pokemon?.abilities?.first?.ability?.name else {
                return "Ability name: -"
            }
            
            return "Ability name:" + abilityName
        }
        
        private func weight() -> String {
            return "Weight:" + String(pokemon?.weight ?? 0)
        }
        
        private func height() -> String {
            return "height:" + String(pokemon?.height ?? 0)
        }
        
        private func typeName() -> String {
            
            guard let typeName =  pokemon?.types?.first?.type?.name else {
                return "Type name: -"
            }
            
            return "Type name:" + typeName
        }
        
    }
}
