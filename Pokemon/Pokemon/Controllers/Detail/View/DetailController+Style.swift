//
//  DetailController+Style.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import UIKit

public extension DetailController {
    
    struct Style {
        public var imageHeight: CGFloat = 220
    }
}

