//
//  DetailController.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import UIKit

public class DetailController: UIViewController {
    
    // MARK: - Properties & outlets
    
    public var name: String?
    
    /// Main stackview holder of the data.
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "Pokemon")
        return imageView
    }()
    
    private let style = DetailController.Style()
    
    private var viewModel: DetailController.ViewModel? {
        didSet {
            setupData()
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create the ViewModel
        self.viewModel = DetailController.ViewModel(name ?? "")
        
        // Setup UI
        setupNavigationBar()
        setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.topItem?.title = name
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    private func setupData() {
        
        self.viewModel?.getPokemonDetails().then(in: .main, { [weak self] list in
            guard let self = self else { return }
            self.view.addSubview(self.stackView)
            self.stackView.centerInSuperview()
            self.setupImage()
            self.setupPokemonInfo()
        })
    }
    
    private func setupImage() {
        
        stackView.addArrangedSubview(imageView)
        imageView.imageFromURL(viewModel?.image())
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: style.imageHeight)
        ])
    }
    
    private func setupPokemonInfo() {
        viewModel?.pokemonInfo().forEach({ info in
            let label = UILabel()
            label.numberOfLines = 0
            label.text = info
            NSLayoutConstraint.activate([
                label.heightAnchor.constraint(equalToConstant: 40)
            ])
            stackView.addArrangedSubview(label)
        })
    }
    
}
