//
//  ListViewModel.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import Foundation
import PokemonAPI
import Hydra

public extension ListController {
    
    class ViewModel: ViewModelProtocol {
        
        // Make protocol happy
        public typealias Model = Void
        public var model: Void
        public required init(_ model: Void) { }
        
        /// Data set.
        public private(set) var list: [PKMNamedAPIResource<PKMPokemon>]?
        
        // MARK: - public functions
        
        // Get the pokemon list
        public func getPokemonList() -> Promise<Void> {
            return fetchPokemonList()
        }
        
        // MARK: - Private functions
        
        // Fetch
        private func fetchPokemonList() -> Promise<Void> {
            
            return Promise<Void>(in: .background) { (resolve, reject, _) in
                PokemonAPI().pokemonService.fetchPokemonList() { [weak self] result in
                    
                    guard let self = self else { return }

                    switch result {
                    case .success(let list):
                        self.list = list.results as? [PKMNamedAPIResource<PKMPokemon>]
                        resolve(Void())
                    case .failure(let error):
                        print(error.localizedDescription)
                        reject(error)
                    }   
                }
            }
        }
        
    }
}
