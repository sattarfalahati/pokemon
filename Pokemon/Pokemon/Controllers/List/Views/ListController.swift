//
//  ListController.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import UIKit

public class ListController: UIViewController {
   
    // MARK: Properties & Outlets
    
    private lazy var table: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.delegate = self
        table.dataSource = self
        return table
    }()
    
    private let style = ListController.Style()
    
    private var viewModel: ListController.ViewModel? {
        didSet {
            setupData()
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create the ViewModel
        self.viewModel = ListController.ViewModel(Void())
        
        // Setup UI
        setupNavigationBar()
        setupUI()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
        
        table.addIntoParent(self.view)
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.topItem?.title = "Pokemons"
    }
    
    private func setupData() {
        self.viewModel?.getPokemonList().then(in: .main, { [weak self] list in
            guard let self = self else { return }
            
            // Reload table
            self.table.reloadData()
        })
    }
}

// MARK: - Table Delegates

extension ListController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.list?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let name = viewModel?.list?[indexPath.row].name
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = name
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return style.cellHeight
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let name = viewModel?.list?[indexPath.row].name
        
        // Go to details
        let detalisVC = DetailController()
        detalisVC.name = name
        self.navigationController?.pushViewController(detalisVC, animated: true)
    }
}
