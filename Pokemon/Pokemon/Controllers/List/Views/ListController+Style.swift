//
//  ListController+Style.swift
//  Pokemon
//
//  Created by sattar.falahati on 01/12/2020.
//

import UIKit

public extension ListController {
    
    struct Style {
        
        public var cellHeight: CGFloat = 70
    }
}
